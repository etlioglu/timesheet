package be.variant.timesheet.menu;

import be.variant.timesheet.domain.day.WorkedDay;
import be.variant.timesheet.domain.slot.Slot;
import be.variant.timesheet.domain.slot.WorkSlot;
import be.variant.timesheet.domain.week.WorkedWeek;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Scanner;

public class Menu {

    //WorkedWeek workedWeek = MenuUtility.createDummyWeek();
    WorkedWeek workedWeek = new WorkedWeek();

    public int getMenuSelection() {
        int selection;
        do {
            MenuUtility.showMenu();
            Scanner keyboard = new Scanner(System.in);
            selection = keyboard.nextInt();
        } while ((selection < 1) && selection > 8);
        return selection;
    }

    public void evaluateSelection() {
        int selection = getMenuSelection();
        switch (selection) {
            case 1:
                MenuUtility.printHourlyRates();
                evaluateSelection();
                break; // not really needed as the evaluateSelection() starts a new loop anyway
            case 2:
                System.out.println();
                System.out.println("A new work-week is to be created, please enter the first day of the work-week.");
                //TODO Check if there is already an instance of a week object,
                // otherwise it will be overwritten
                workedWeek = MenuUtility.createWeek(MenuUtility.inputDate());
                evaluateSelection();
                break;
            case 3:
                System.out.println();
                System.out.println("You would like enter a new slot");
                enterNewTimeSlot();
                evaluateSelection();
                break;
            case 4:
                System.out.println();
                System.out.println("You would like to delete an existing slot");
                deleteExistingTimeSlot();
                evaluateSelection();
                break;
            case 5:
                System.out.println();
                System.out.println("Resetting the current week");
                reset();
                evaluateSelection();
                break;
            case 6:
                System.out.println();
                System.out.println("Paycheck: ");
                printPaycheck();
                evaluateSelection();
                break;
            case 7:
                // duplicates the code at case6 until printDetailedPaycheck() is implemented
                System.out.println();
                System.out.println("Paycheck: ");
                printPaycheck();
                evaluateSelection();
                break;
            case 8:
                System.out.println("Exiting the application");
                System.exit(0);
                break;
            default:
                evaluateSelection();
        }
    }

    public void reset() {
        System.out.println();
        if (!isThereAConstructedWeek()) {
            evaluateSelection();
        } else {
            for (Object day: workedWeek) {
                WorkedDay castedDay = (WorkedDay) day;
                for (Object slot: castedDay.getTimeSlots()) {
                    Slot castedSlot = (Slot) slot;
                    LocalTime slotStart = castedSlot.getStart();
                    LocalTime slotEnd = castedSlot.getEnd();
                    castedDay.removeSlot(slotStart, slotEnd);
                }
            }
        }
    }

    public void enterNewTimeSlot() {
        System.out.println();
        if (!isThereAConstructedWeek()) {
            evaluateSelection();
        } else {
            LocalDate newSlotDate = MenuUtility.inputDate();
            if (!isDayInWorkWeek(newSlotDate)) {
                evaluateSelection();
            } else {
                for (Object day : workedWeek) {
                    WorkedDay castedDay = (WorkedDay) day;
                    if (castedDay.getDateOfWorkingDay().equals(newSlotDate)) {
                        System.out.println("These many time slots are already present for " + newSlotDate + ": " + castedDay.getTimeSlots().length);
                        castedDay.printTimeSlots();

                        System.out.println();
                        System.out.print("Enter the start time of the new slot: ");
                        LocalTime newSlotStart = MenuUtility.inputTime();

                        System.out.print("Enter the end time of the new slot: ");
                        LocalTime newSlotEnd = MenuUtility.inputTime();

                        //TODO: Currently only work slots can be entered
                        Slot newTimeSlot = new WorkSlot(newSlotStart, newSlotEnd);

                        castedDay.addSlot(newTimeSlot);

                        System.out.println();
                        System.out.println("Current number of time slots on " + newSlotDate + ": " + castedDay.getTimeSlots().length);
                        castedDay.printTimeSlots();
                        System.out.println();
                    }
                }
            }
        }
    }

    public void deleteExistingTimeSlot() {
        System.out.println();
        if (!isThereAConstructedWeek()) {
            evaluateSelection();
        } else {
            LocalDate dateOfSlot = MenuUtility.inputDate();
            if (!isDayInWorkWeek(dateOfSlot)) {
                evaluateSelection();
            } else {
                for (Object day : workedWeek) {
                    WorkedDay castedDay = (WorkedDay) day;
                    if (castedDay.getDateOfWorkingDay().equals(dateOfSlot)) {
                        System.out.println("These many time slots are already present for " + dateOfSlot + ": " + castedDay.getTimeSlots().length);
                        castedDay.printTimeSlots();

                        System.out.println();
                        System.out.print("Enter the start time of the slot to be removed: ");
                        LocalTime newSlotStart = MenuUtility.inputTime();

                        System.out.print("Enter the end time of the slot to be removed: ");
                        LocalTime newSlotEnd = MenuUtility.inputTime();

                        //TODO: Currently only work slots can be entered
                        Slot oldTimeSlot = new WorkSlot(newSlotStart, newSlotEnd);

                        if (castedDay.slotExistsInDay(oldTimeSlot)) {
                            castedDay.removeSlot(newSlotStart, newSlotEnd);
                        } else {
                            System.out.println("This slot does not exist so cannot be removed...");
                            System.out.println();
                            evaluateSelection();
                        }

                        System.out.println();
                        System.out.println("Current number of time slots on " + dateOfSlot + ": " + castedDay.getTimeSlots().length);
                        castedDay.printTimeSlots();
                        System.out.println();
                    }
                }
            }
        }
    }



    public boolean isThereAConstructedWeek() {

        if (workedWeek.getFirstDayOfWeek().getDateOfWorkingDay().equals(MenuUtility.dummyDate)) {
            System.out.println("No work-week has been created yet, please select option 2 from the main menu and create a work-wek first.");
            System.out.println();
            return false;
        } else {
            return true;
        }
    }

    public boolean isDayInWorkWeek(LocalDate date) {

        if (date.isBefore(workedWeek.getFirstDayOfWeek().getDateOfWorkingDay()) ||
                date.isAfter(workedWeek.getFirstDayOfWeek().getDateOfWorkingDay().plusDays(7))) {
            System.out.println("You can only work with a time-slot that is within the current work-week...");
            System.out.println();
            return false;
        } else {
            return true;
        }

    }

    public void printPaycheck() {
        if (isThereAConstructedWeek()) {
            // looping through the days of the work-week
            int wageWeek = 0;
            for (Object day : workedWeek) {

                WorkedDay castedDay = (WorkedDay) day;

                System.out.println();
                System.out.print("Workday " + castedDay.getDateOfWorkingDay() + ", a " + castedDay.getDateOfWorkingDay().getDayOfWeek().toString().toLowerCase());
                System.out.println();

                // "day of the work-day" should be in String type for the switch-case below
                String dayToCalculate = castedDay.getDateOfWorkingDay().getDayOfWeek().toString();

                // getting wages based on the day
                // the two wage variables had to be initialized, declaration was not enough
                double normalWage = 0.0;
                double extraWage = 0.0;

                // define normal- and over-work hours
                LocalTime eightAM = LocalTime.of(8, 0);
                LocalTime sixPM = LocalTime.of(18, 0);

                switch (dayToCalculate) {

                    // saturday and sunday would be enough, the rest would go in the default, so less typing
                    case "MONDAY":
                    case "TUESDAY":
                    case "WEDNESDAY":
                    case "THURSDAY":
                    case "FRIDAY":
                        normalWage = 15.0;
                        extraWage = 20.0;
                        break;
                    case "SATURDAY":
                        normalWage = 25.0;
                        extraWage = 25.0;
                        //System.out.println("norm " + normalWage);
                        break;
                    case "SUNDAY":
                        normalWage = 30.0;
                        extraWage = 30.0;
                        //System.out.println("norm " + normalWage);
                        break;
                }

                System.out.println("Wage for normal hours for this day: " + normalWage + ", Wage for over hours for this day: " + extraWage);

                double wageDay = 0.0;
                for (Object slot : castedDay.getTimeSlots()) {
                    Slot castedSlot = (Slot) slot;

                    LocalTime slotStart = castedSlot.getStart();
                    LocalTime slotEnd = castedSlot.getEnd();

                    double wageSlot = 0.0;

                    // slot ends before or starts after normal work hours
                    if (
                            ((slotEnd.isBefore(eightAM)) || (slotEnd.equals(eightAM))) ||
                                    ((slotStart.isAfter(sixPM)) || (slotStart.equals(sixPM)))
                    ) {
                        wageSlot += castedSlot.totalMinutes() * extraWage;
                    }

                    // slot starts before normal work hours but end within normal work hours
                    else if (
                            (slotStart.isBefore(eightAM)) && (slotEnd.isBefore(sixPM))
                    ) {
                        wageSlot += (castedSlot.totalMinutes() * normalWage) +
                                (Duration.between(slotStart, eightAM).toMinutes() * extraWage);
                    }

                    // slot starts within normal work hours but end after normal work hours
                    else if (
                            (slotStart.isAfter(eightAM)) && (slotEnd.isAfter(sixPM))
                    ) {
                        wageSlot += (castedSlot.totalMinutes() * normalWage) +
                                (Duration.between(sixPM, slotEnd).toMinutes() * extraWage);
                    }
                    // slot starts before normal work hours and end after normal work hours
                    else if (
                            (slotStart.isBefore(eightAM)) && (slotEnd.isAfter(sixPM))
                    ) {
                        wageSlot += (castedSlot.totalMinutes() * normalWage) +
                                (Duration.between(slotStart, eightAM).toMinutes() * extraWage) +
                                (Duration.between(sixPM, slotEnd).toMinutes() * extraWage);
                    }
                    // slot is within normal hours
                    else if (
                            ((slotStart.equals(eightAM)) || (slotStart.isAfter(eightAM))) &&
                                    ((slotEnd.equals(sixPM)) || (slotEnd.isBefore(sixPM)))
                    ) {
                        wageSlot += (castedSlot.totalMinutes() * normalWage);
                    } else {
                        System.out.println("Error!!!!!");
                        System.out.println("Slot start: " + slotStart + ", Slot end: " + slotEnd);
                    }

                    wageDay += wageSlot;
                    System.out.println("Slot " + slotStart + " - " + slotEnd + " => " + (wageSlot / 60));
                }
                System.out.println("Total wage for this day: " + wageDay / 60);
                wageWeek += wageDay;


            }
            System.out.println();
            System.out.println("Total wage for the work-week: " + wageWeek / 60);
            System.out.println();
        }
    }
}

