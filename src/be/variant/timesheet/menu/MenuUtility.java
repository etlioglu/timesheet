package be.variant.timesheet.menu;

import be.variant.timesheet.domain.salary.Rates;
import be.variant.timesheet.domain.week.WorkedWeek;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuUtility {

    static LocalDate dummyDate = LocalDate.of(1871, 3, 18);


    static void showMenu() {
        System.out.println("Main menu, please select an action by entering a number...");
        System.out.println();
        System.out.println("1 - Show the hourly rates");
        System.out.println("2 - Start a new workweek (Two slots are automatically added to each day of the week)");
        System.out.println("3 - Add a slot");
        System.out.println("4 - Delete a slot* ");
        System.out.println("5 - Reset*");
        System.out.println("6 - Print paycheck");
        System.out.println("7 - Print detailed paycheck (not implemented yet, does the same as the \"Print paycheck\")");
        System.out.println("8 - Quit application");
        System.out.println();
        System.out.println("* Slots are not deleted but the end time is set to the start time, a workaround until ArrayList is implemented");
        System.out.println();
        System.out.print("Your choice: ");
    }

    static LocalDate inputDate() {

        int year = 1;
        int month = 1;
        int day = 1;
        LocalDate date = LocalDate.of(year, month, day);

        Scanner keyboard = new Scanner(System.in);

        System.out.println();
        System.out.println("The date should be entered as three integers, for the year, month and day respectively.");

        try {
            System.out.print("Year: ");
            year = keyboard.nextInt();
            System.out.print("Month: ");
            month = keyboard.nextInt();
            System.out.print("Day: ");
            day = keyboard.nextInt();
            date = LocalDate.of(year, month, day);
        } catch (InputMismatchException | DateTimeException ex) {
            System.out.println("Year, month and day should be entered as integers and in the range 0-9999, 1-12, 1-31 respectively");
            inputDate();
        }

        System.out.println();

        return date;

    }

    static LocalTime inputTime() {

        // I had to declare these here as scope regulations do not allow to declare these within a try-catch
        int hour = 0;
        int minute = 0;
        LocalTime time = LocalTime.of(hour, minute);

        Scanner keyboard = new Scanner(System.in);

        System.out.println();
        System.out.println("The time should be entered as two integers, for the hour and minute respectively.");

        try {
            System.out.print("Hour: ");
            hour = keyboard.nextInt();
            System.out.print("Minute: ");
            minute = keyboard.nextInt();
            time = LocalTime.of(hour, minute);
        } catch (InputMismatchException | DateTimeException ex) {
            System.out.println("Hour should be in the range 0-23 and minute should be in the range 0-59");
            inputTime();
        }

        System.out.println();

        return time;

    }

    static WorkedWeek createDummyWeek() {

        LocalDate firstDayOfWeek = LocalDate.of(1970, 1, 1);

        WorkedWeek workedWeek = new WorkedWeek(firstDayOfWeek);

        return workedWeek;
    }

    static WorkedWeek createWeek(LocalDate firstDayOfWeek) {

        WorkedWeek workedWeek = new WorkedWeek(firstDayOfWeek);

        return workedWeek;
    }

    static void printHourlyRates() {
        System.out.println();
        System.out.println("Hourly rates:");
        System.out.println();
        Rates.printRates();
        System.out.println();
    }

}
