package be.variant.timesheet.domain.slot;

import java.time.LocalTime;

public class BreakSlot extends Slot{

    private static final String slotType = "break";

    public BreakSlot(LocalTime start, LocalTime end) {
        super(start, end);
    }

    public BreakSlot(LocalTime start, LocalTime end, String description) {
        super(start, end, description);
    }

    public static String getSlotType() {
        return slotType;
    }
}
