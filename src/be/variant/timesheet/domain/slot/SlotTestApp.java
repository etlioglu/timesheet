package be.variant.timesheet.domain.slot;

import java.time.LocalTime;

public class SlotTestApp {

    public static void main(String[] args) {

        LocalTime start = LocalTime.of(9, 0);
        LocalTime end = LocalTime.of(10, 0);

        WorkSlot timeSlot = new WorkSlot(start, end);
        BreakSlot breakSlot = new BreakSlot(start, end);

        System.out.println(timeSlot.totalMinutes() + " and " + timeSlot.getSlotType());
        System.out.println(breakSlot.totalMinutes() + " and " + breakSlot.getSlotType());

    }

}
