package be.variant.timesheet.domain.slot;

import java.time.LocalTime;

public class WorkSlot extends Slot{

    private static final String SLOT_TYPE = "work";

    public WorkSlot(LocalTime start, LocalTime end) {
        super(start, end);
    }

    public WorkSlot(LocalTime start, LocalTime end, String description) {
        super(start, end, description);
    }

    public static String getSlotType() {
        return SLOT_TYPE;
    }
}
