package be.variant.timesheet.domain.slot;

import java.time.LocalTime;
import java.time.Duration;

public abstract class Slot {

    public LocalTime start;
    public LocalTime end;
    String description;

    public Slot(LocalTime start, LocalTime end) {
        this(start, end, "No description provided for this slot.");
    }

    public Slot(LocalTime start, LocalTime end, String description) {
        checkHours();
        setStart(start);
        setEnd(end);
        setDescription(description);
    }

    public void checkHours() {

    }

    public long totalMinutes() {
        return Duration.between(start, end).toMinutes();
    }

    public long[] minutesByType() {
        return null;
    }

    void printSlotInfo() {
        System.out.println("Slot information");
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}