package be.variant.timesheet.domain.week;

import be.variant.timesheet.domain.day.WorkedDay;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;

public class WorkedWeek implements Iterable {

    /**
     * This is a class that is composed of 7 days, which are represented by the WorkedWeek() class.
     *
     *
     * @author H. Emre Etlioglu
     * @version 0.1
     * @since 2021-03-18
     */
    
    WorkedDay[] workedWeek = new WorkedDay[7];

    public WorkedWeek() {
        // 1871-03-18 is the day Paris Commune starts
        /** This can be used to construct a dummy week */
        this(LocalDate.of(1871, 3, 18));
    }

    public WorkedWeek(LocalDate dateOfFirstDay) {

        LocalDate dateToCreate = dateOfFirstDay;
        for (int i = 0; i < 7; i++) {
            WorkedDay newWorkedDay = new WorkedDay(dateToCreate);
            workedWeek[i] = newWorkedDay;
            dateToCreate = dateToCreate.plus(1, ChronoUnit.DAYS);
        }
    }


    @Override
    public Iterator iterator() {
        return new WorkedWeekIterator();
    }

    private class WorkedWeekIterator implements Iterator {

        private int index = 0;

        @Override
        public boolean hasNext() {
            for (int i = index; i < workedWeek.length; i++) {
                if (workedWeek[i] != null) {
                    index = i;
                    return true;
                }
            }
            return false;
        }

        @Override
        public Object next() {
            for (int i = index; i < workedWeek.length; i++) {
                if (workedWeek[i] != null) {
                    return workedWeek[index++];
                }
            }
            return null;
        }
    }

    public WorkedDay getFirstDayOfWeek() {
        return workedWeek[0];
    }


}
