package be.variant.timesheet.domain.salary;

import be.variant.timesheet.domain.day.WorkedDay;
import be.variant.timesheet.domain.slot.Slot;
import be.variant.timesheet.domain.week.WorkedWeek;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

public class SalaryCalculator {

    public static void main(String[] args) {

        LocalDate testDay = LocalDate.of(2021, 1, 1);
        WorkedWeek testWeek = new WorkedWeek(testDay);

        System.out.println("Paycheck details: ");

        // looping through the days of the work-week
        for (Object day : testWeek) {

            WorkedDay castedDay = (WorkedDay) day;

            System.out.println();
            System.out.print("Workday " + castedDay.getDateOfWorkingDay() + ", a " + castedDay.getDateOfWorkingDay().getDayOfWeek().toString().toLowerCase());
            System.out.println();

            // "day of the work-day" should be in String type for the switch-case below
            String dayToCalculate = castedDay.getDateOfWorkingDay().getDayOfWeek().toString();

            // getting wages based on the day
            // the two wage variables had to be initialized, declaration was not enough
            double normalWage = 0.0;
            double extraWage = 0.0;

            // define normal- and over-work hours
            LocalTime nineAM = LocalTime.of(9, 0);
            LocalTime sixPM = LocalTime.of(18, 0);

            switch (dayToCalculate) {

                // saturday and sunday would be enough, the rest would go in the default, so less typing
                case "MONDAY":
                case "TUESDAY":
                case "WEDNESDAY":
                case "THURSDAY":
                case "FRIDAY":
                    normalWage = 15.0;
                    extraWage = 20.0;
                    break;
                case "SATURDAY":
                    normalWage = 25.0;
                    extraWage = 25.0;
                    //System.out.println("norm " + normalWage);
                    break;
                case "SUNDAY":
                    normalWage = 30.0;
                    extraWage = 30.0;
                    //System.out.println("norm " + normalWage);
                    break;
            }

            System.out.println("Wage for normal hours for this day: " + normalWage + ", Wage for over hours for this day: " + extraWage);

            double wageDay = 0.0;
            for (Object slot : castedDay.getTimeSlots()) {
                Slot castedSlot = (Slot) slot;

                LocalTime slotStart = castedSlot.getStart();
                LocalTime slotEnd = castedSlot.getEnd();

                double wageSlot = 0.0;

                // slot ends before or starts after normal work hours
                if (
                        ((slotEnd.isBefore(nineAM)) || (slotEnd.equals(nineAM))) ||
                                ((slotStart.isAfter(sixPM)) || (slotStart.equals(sixPM)))
                ) {
                    wageSlot += castedSlot.totalMinutes() * extraWage;
                }

                // slot starts before normal work hours but end within normal work hours
                else if (
                        (slotStart.isBefore(nineAM)) && (slotEnd.isBefore(sixPM))
                ) {
                    wageSlot += (castedSlot.totalMinutes() * normalWage) +
                            (Duration.between(slotStart, nineAM).toMinutes() * extraWage);
                }

                // slot starts within normal work hours but end after normal work hours
                else if (
                        (slotStart.isAfter(nineAM)) && (slotEnd.isAfter(sixPM))
                ) {
                    wageSlot += (castedSlot.totalMinutes() * normalWage) +
                            (Duration.between(sixPM, slotEnd).toMinutes() * extraWage);
                }
                // slot starts before normal work hours and end after normal work hours
                else if (
                        (slotStart.isBefore(nineAM)) && (slotEnd.isAfter(sixPM))
                ) {
                    wageSlot += (castedSlot.totalMinutes() * normalWage) +
                            (Duration.between(slotStart, nineAM).toMinutes() * extraWage) +
                            (Duration.between(sixPM, slotEnd).toMinutes() * extraWage);
                }
                // slot is within normal hours
                else if (
                        ((slotStart.equals(nineAM)) || (slotStart.isAfter(nineAM))) &&
                                ((slotEnd.equals(sixPM)) || (slotEnd.isBefore(sixPM)))
                ) {
                    wageSlot += (castedSlot.totalMinutes() * normalWage);
                }
                else {
                    System.out.println("Error!!!!!");
                    System.out.println("Slot start: " + slotStart + ", Slot end: " + slotEnd);
                }

                wageDay += wageSlot;
                System.out.println("Slot " + slotStart + " - " + slotEnd + " => " + (wageSlot / 60));
            }
            System.out.println("Total wage for this day: " + wageDay / 60);
            System.out.println();



        }


    }
}
