package be.variant.timesheet.domain.salary;

public enum Rates {
    MONDAY(15.0, 20.0),
    TUESDAY(15.0, 20.0),
    WEDNESDAY(15.0, 20.0),
    THURSDAY(15.0, 20.0),
    FRIDAY(15.0, 20.0),
    SATURDAY(25.0, 25.0),
    SUNDAY(30.0, 30.0);

    private final double normalHourlyRate;
    private final double overtimeHourlyRate;


    Rates(double normalHourlyRate, double overtimeHourlyRate) {
        this.normalHourlyRate = normalHourlyRate;
        this.overtimeHourlyRate = overtimeHourlyRate;
    }

    public double getNormalHourlyRate() {
        return normalHourlyRate;
    }

    public double getOvertimeHourlyRate() {
        return overtimeHourlyRate;
    }

    public static void printRates() {
        for (Rates day : Rates.values()) {
            System.out.println(day + "; " + "Normal hours: " + day.normalHourlyRate + " Euro, Over-hours: " + day.overtimeHourlyRate + " Euro.");
        }
    }
}
