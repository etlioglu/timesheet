package be.variant.timesheet.domain.day;

import be.variant.timesheet.domain.slot.Slot;
import be.variant.timesheet.domain.slot.WorkSlot;

import java.time.LocalDate;
import java.time.LocalTime;

public class WorkedDayTestApp {

    public static void main(String[] args) {

        LocalDate testDate = LocalDate.of(2021, 1,1);

        LocalTime testTimeStart = LocalTime.of(9, 0);
        LocalTime testTimeEnd= LocalTime.of(13, 0);
        Slot testSlot = new WorkSlot(testTimeStart, testTimeEnd);

        WorkedDay testDay = new WorkedDay(testDate);

        System.out.println(testDay.getDateOfWorkingDay());
        testDay.printTimeSlots();

        testDay.addSlot(testSlot);

        testDay.printTimeSlots();



    }
}
