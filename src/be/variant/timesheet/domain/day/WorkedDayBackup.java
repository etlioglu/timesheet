//package be.variant.timesheet.service.day;
//
//import be.variant.timesheet.service.slot.BreakSlot;
//import be.variant.timesheet.service.slot.Slot;
//import be.variant.timesheet.service.slot.WorkSlot;
//
//import java.time.LocalDate;
//import java.time.LocalTime;
//import java.util.Iterator;
//
//public class WorkedDayBackup implements Day, Iterable {
//
//    public static final int MAX = 5;
//    LocalDate dateOfWorkedDay;
//    Slot[] timeSlots = new Slot[MAX]; // some arbitrarily large number of slots per day
//
//    public WorkedDayBackup(LocalDate date) {
//
//        setDateOfWorkedDay(date);
//        setTimeSlots(timeSlots);
//
//        addSlot(createSlotWithIntAndString(9, 12, "work"));
//        addSlot(createSlotWithIntAndString(12, 13, "break"));
//        addSlot(createSlotWithIntAndString(13, 17, "work"));
//
//    }
//
//    public Iterator iterator() {
//        return new WorkedDayIterator();
//    }
//
//    public Slot createSlotWithIntAndString(int start, int end, String slotType) {
//
//        LocalTime slotStart = LocalTime.of(start, 0);
//        LocalTime slotEnd = LocalTime.of(end, 0);
//
//        if (slotType.equals("work")) {
//            return new WorkSlot(slotStart, slotEnd);
//        } else {
//            return new BreakSlot(slotStart, slotEnd);
//        }
//
//    }
//
//    public Slot[] getTimeSlots() {
//        return timeSlots;
//    }
//
//    public void setTimeSlots(Slot[] timeSlots) {
//        this.timeSlots = timeSlots;
//    }
//
//    public LocalDate getDateOfWorkedDay() {
//        return dateOfWorkedDay;
//    }
//
//    public void setDateOfWorkedDay(LocalDate dateOfWorkedDay) {
//        this.dateOfWorkedDay = dateOfWorkedDay;
//    }
//
//    @Override
//    public void removeSlot() {
//
//    }
//
//    @Override
//    public void addSlot(Slot individualTimeSlot) {
//
//        for (int i = 0; i < timeSlots.length; i++) {
//            if (timeSlots[i] == null) {
//                timeSlots[i] = individualTimeSlot;
//                System.out.println("Added to " + i);
//                break;
//            } else {
//                System.out.println("nothing was added");
//                System.out.println("There can only be " + MAX + " work or break slots in a given day!");
//            }
//        }
//
//    }
//
//    @Override
//    public long totalWorkedMinutes() {
//        return 0;
//    }
//
//    private class WorkedDayIterator implements Iterator {
//
//        private int index = 0;
//
//        @Override
//        public boolean hasNext() {
//            for (int i = index; i < timeSlots.length; i++) {
//                if (timeSlots[i] != null) {
//                    index = i;
//                    return true;
//                }
//            }
//            return false;
//        }
//
//        @Override
//        public Object next() {
//
//            for (int i = index; i < timeSlots.length; i++) {
//                if (timeSlots[i] != null) {
//                    return timeSlots[index++];
//                }
//            }
//            return null;
//        }
//    }
//}
