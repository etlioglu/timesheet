package be.variant.timesheet.domain.day;

import be.variant.timesheet.domain.slot.BreakSlot;
import be.variant.timesheet.domain.slot.Slot;
import be.variant.timesheet.domain.slot.WorkSlot;

import java.time.LocalDate;
import java.time.LocalTime;

public class WorkedDay {


    LocalDate dateOfWorkingDay;
    Slot[] timeSlots = {};

    public WorkedDay(LocalDate date) {

        setDateOfWorkingDay(date);
        setTimeSlots(timeSlots);

        addSlot(createSlotWithIntAndString(9, 12, "work"));
        //addSlot(createSlotWithIntAndString(12, 13, "work"));
        addSlot(createSlotWithIntAndString(19, 21, "work"));


    }

    public LocalDate getDateOfWorkingDay() {
        return dateOfWorkingDay;
    }

    public void setDateOfWorkingDay(LocalDate dateOfWorkingDay) {
        this.dateOfWorkingDay = dateOfWorkingDay;
    }

    public Slot[] getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(Slot[] timeSlots) {
        this.timeSlots = timeSlots;
    }

    public void printTimeSlots() {
        for (Slot timeSlot : timeSlots) {
            //Slot timeSlot = (Slot) el;
            System.out.println("Slot start: " + timeSlot.start + ", Slot end: " + timeSlot.end);
        }
    }

    public boolean slotExistsInDay(Slot timeSlot) {
        // check if the slot to be removed is in the array
        boolean slotExits = false;
        int index = -1;
        for (Object slot : timeSlots) {
            index += 1;
            Slot castedSlot = (Slot) slot;
            if ((castedSlot.getStart().equals(timeSlot.getStart())) &&
                    (castedSlot.getEnd().equals(timeSlot.getEnd()))
            ) {
                slotExits = true;
                break;
            }
        }
        return slotExits;

    }

    public Slot[] addSlot(Slot individualTimeSlot) {

        checkAndModifySlotsArray(individualTimeSlot);

        //ArrayList<Slot> temp = new ArrayList<Slot>(Arrays.asList(timeSlot));
        Slot[] tempArray = new Slot[timeSlots.length + 1];
        System.arraycopy(timeSlots, 0, tempArray, 0, timeSlots.length);
        tempArray[timeSlots.length] = individualTimeSlot;
        timeSlots = tempArray;
        return timeSlots;
    }

    public Slot[] removeSlot(LocalTime startTime, LocalTime endTime) {
        /**
         * Does not really remove a Slot object from a Slot array but sets the end time to the start time.
         *
         */

        for (Object el : timeSlots) {
            Slot castedSlot = (Slot) el;

            if ((castedSlot.getStart() == startTime) &&
                    castedSlot.getEnd() == endTime) {
                castedSlot.setEnd(castedSlot.getStart());
            }
        }
        return timeSlots;
    }

    public Slot checkAndModifySlotsArray(Slot individualTimeSlot) {
        for (Object el : timeSlots) {
            Slot castedSlot = (Slot) el;

            LocalTime startNew = individualTimeSlot.getStart();
            LocalTime endNew = individualTimeSlot.getEnd();

            LocalTime startExisting = castedSlot.getStart();
            LocalTime endExisting = castedSlot.getEnd();

            // new slot ends before or right at the start of existing slots, cases 1 and 2
            if ((endNew.isBefore(startExisting)) || (endNew.equals(startExisting))) {
                individualTimeSlot = individualTimeSlot;
                // System.out.println(1);
            }
            // new slot starts before existing and ends within existing, cases 3, 4, 5
            else if (((endNew.isBefore(endExisting)) || (endNew.equals(endExisting))) &&
                            ((startNew.isBefore(startExisting)) || (startNew.equals(startExisting)))) {
                individualTimeSlot.setEnd(startExisting);
                // System.out.println(2);

                // new slot is within existing, cases 10, 11
            } else if (
                    ((startNew.isAfter(startExisting)) || (startNew.equals(startExisting))) &&
                            ((endNew.isBefore(endExisting)) || (endNew.equals(endExisting)))
            ) {
                individualTimeSlot.setEnd(individualTimeSlot.getStart());
                // System.out.println(3);

            }
            // new slot is longer than existing, case 6
            else if (
                    (startNew.isBefore(startExisting)) && (endNew.isAfter(endExisting))
            ) {
                removeSlot(startExisting, endExisting);
                // System.out.println(4);
            }

            // new slot starts in existing and ends after existing
            else if ((endNew.isAfter(endExisting)) &&
                    ((startNew.isBefore(endExisting)) || (startNew.equals(startExisting)))) {
                individualTimeSlot.setStart(endExisting);
                System.out.println(5);
            }

            // new slot is starts and ends after existing
            else if (
                    (startNew.isAfter(endExisting)) || (startNew.equals(endExisting))
            ) {
                individualTimeSlot = individualTimeSlot;
                //System.out.println("6");
            } else {
                System.out.println("Error!");
            }
        }
        return individualTimeSlot;
    }


    public Slot createSlotWithIntAndString(int start, int end, String slotType) {

        LocalTime slotStart = LocalTime.of(start, 0);
        LocalTime slotEnd = LocalTime.of(end, 0);

        if (slotType.equals("work")) {
            return new WorkSlot(slotStart, slotEnd);
        } else {
            return new BreakSlot(slotStart, slotEnd);
        }

    }


}